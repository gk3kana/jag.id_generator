﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JAG.ID_Generator
{
    class IDGenerator
    {
		public IDGenerator() {}

		public string CheckDigit(string idNumber)
		{
			// Luhn algorithm : https://en.wikipedia.org/wiki/Luhn_algorithm
			string		concat = null,
						Bx2 = null,
						sZ = null;

			int			i = 0, A = 0, 
						B = 0, C = 0, 
						D = 0, Z = 0;
			
			if (idNumber == null)
				return null;
			try { 
				for (i = 0; i < idNumber.Length; i++) {
					string tmp = idNumber[i].ToString();
					if ((i + 1) % 2 != 0)
						A += int.Parse(tmp);			// 1. A = sum of all odd numbered digits
					else
						concat += tmp;					// 2. B = concat all even numbered digits
				}
				B = int.Parse(concat);
				Bx2	= (B * 2).ToString(); 
				for (i = 0; i < Bx2.Length; i++)
					C += int.Parse(Bx2[i].ToString());	// 3. C = sum of digits in (2 * B)
				D = A + C;								// 4. D = A + C
				Z = (10 - (D % 10));					// 5. Z = 10 - (D mod 10)
			} catch (Exception e) {
				Console.WriteLine(e);
			}
			
			sZ = Z.ToString();
			return (sZ[sZ.Length - 1].ToString());		// return last digit of Z
		}

		public string GenerateID()
		{
			Random		rnd;
			int			year, month, day, sequence, race, citizenship;
			string		sMonth, sDay, idNumber = "";

			try {
				rnd = new Random();
				year = rnd.Next(10, 99);
				month = rnd.Next(1, 12);
				race = rnd.Next(8, 9);
				day = rnd.Next(1, 30);

				// add zero in front of month and day if they are 1 digit
				sMonth = (month < 10) ? ("0" + month.ToString()) : (month.ToString());
				sDay = (day < 10) ? ("0" + day.ToString()) : (day.ToString());
				sequence = rnd.Next(1000, 9999);
				citizenship = rnd.Next(0, 1);

				// YYMMDD G SSS	C R (w/o check digit) 
				idNumber = year + sMonth +
								sDay + sequence	+
								citizenship + race.ToString();

				idNumber += this.CheckDigit(idNumber);

			} catch (Exception e) {
				Console.WriteLine("Error generating ID number: " + e);
			}
			return (idNumber);
		}

		static void Main(string[] args)
        {
			Console.WriteLine((new IDGenerator()).GenerateID());
			Console.ReadKey(); // press start to continue 	
        }
    }
}
